
(function(root)
{
	'use strict'

	function slugify(string)
	{
		return string.toString().toLowerCase()
			.replace(/\s+/g, '-')				// Replace spaces with -
			.replace(/[^\w\-]+/g, '')			// Remove all non-word chars
			.replace(/\-\-+/g, '-')				// Replace multiple - with single -
			.replace(/^-+/, '')					// Trim - from start of text
			.replace(/-+$/, '');				// Trim - from end of text
	}

	if (typeof exports === 'object')
	{
		// CommonJS
		module.exports			= slugify;
	}
	else if (typeof define === 'function' && define.amd)
	{
		// AMD. Register as an anonymous module.
		define([], function()
		{
			return slugify;
		});
	}
	else
	{
		// Browser globals
		root.slugify			= slugify;
	}
}(this));
